FROM openjdk:8u121-jdk
ARG CI_PROJECT_DIR
ENV CI_PROJECT_DIR=${CI_PROJECT_DIR}
VOLUME /tmp
RUN echo $CI_PROJECT_DIR

COPY target/actuator-sample-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
